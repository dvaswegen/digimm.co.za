
$(function() {
	$.stellar({
		horizontalScrolling: false,
		horizontalOffset: 0,
		verticalOffset: 0,
		responsive: true
	});
});

$(function() {
	$("form.validate-me").each(function() {
		$(this).validate();
	});
});

$(function() {
	$("[data-scrollto]").each(function() {
		$(this).on('click', function(e) {
			e.preventDefault();
			$('html,body').stop().animate({scrollTop: $($(this).data('scrollto')).offset().top - 20}, 700);
			return false;
		});
	})
})

$(function () {

	var $body = $('html, body')
	var $window = $(window);

	var options = {
		countElementPartially: true,
		classes: {
			inView: 'in-view',
			notInView: 'not-in-view'
		},
		scrollfactor: 1
	};

	$('[data-parallax]').each(function() {

		var elem = this;
		var $elem = $(elem);
		$elem.wrap('<div class="parallax-wrapper"></div>');
		var $wrap = $elem.parent('.parallax-wrapper');

		var scrollfactor = $elem.data('parallax') || 1;
		var scrolloffset = $elem.data('parallax-offset') || 0;

		$wrap.css('position', 'relative');
		resetPosition();

		function resetPosition () {
			$wrap.css('top', '0px');
		}
		function changePositionTo (to) {
			$wrap.css('top', to + 'px');
		}

		function elemScroll () {

			if(window.innerWidth < 768) {
				return;
			}
			else {

				var winHeight = window.innerHeight;
				var winScrollTop = $window.scrollTop();
				var winScrollBot = winScrollTop + winHeight;

				var originalOffset = $wrap.offset().top;

				// check in-view 
				var elementIsInView = true;
				var elemOffTop = $wrap.offset().top;
				var elemOffBot = $wrap.innerHeight() + elemOffTop;
				if(options.countElementPartially) {
					elementIsInView = (elemOffBot >= winScrollTop) && (elemOffTop <= winScrollBot);
				}
				else {
					elementIsInView = (elemOffTop >= winScrollTop) && (elemOffBot <= winScrollBot);
				}

				// do classes
				if(elementIsInView) {
					if(!$elem.hasClass(options.classes.inView)) {
						$elem.addClass(options.classes.inView);
						$elem.removeClass(options.classes.notInView);
					}
				}
				else {
					if($elem.hasClass(options.classes.inView)) { 
						$elem.removeClass(options.classes.inView);
						$elem.addClass(options.classes.notInView);
					}
					else {
						$elem.addClass(options.classes.notInView);
					}
				}

				if(elementIsInView) {
					changePositionTo(((0 + winScrollTop) * scrollfactor) - winScrollTop + scrolloffset);
				}
			}
		}

		$window.on('scroll', elemScroll);
		elemScroll();
	});

});



WebFont.load({
	google: {
		families: ['Lato:100,300,400,700']
	}
});

