<?php 
$pageTitle = 'Home';
?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_top.php' ?>

<div id="header" class="header-home noverflow" data-stellar-background-ratio="0.7">
	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navtop.php' ?>

	<div id="slogan" data-parallax="1.2">
		<h1><b>Where businesses come to grow</b></h1>
		<a href="#" class="btn btn-default" data-scrollto="#home_oppertunities">
			GET STARTED
		</a>
	</div>
</div>

<section id="home_intro">
	<div class="container text-center"> 
		<h2 class="heading">
			The authority on selling high return low investment business in South&nbsp;Africa
		</h2>
		<hr class="line">
		<p class="paragraph">
			Our business opportunities have a successful established reputation with proven management and ongoing support. Purchasing one of our business opportunities is seen by many as a simple way to go into business for the first&nbsp;time.
		</p>
	</div>
</section>

<section id="home_oppertunities" data-stellar-background-ratio="0.8">
	<div class="container text-center" data-parallax="1.1" data-parallax-offset="-75">
		<h2 class="heading">
			We offer the independence of small business ownership supported by benefits of a big business network.
		</h2>
		<hr class="line">
		<p class="paragraph">
			Our agents have a higher rate of success than start-up businesses, so what are you waiting for, join our family and be an owner we can count on!
		</p>
		<a href="/business-opportunities" class="btn btn-default">
			VIEW OPPERTUNITIES
		</a>
	</div>
	<div id="boxes" data-parallax="0.88" data-parallax-offset="200">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="box one">
						<h2>Ideal for first&#8209;timers</h2>
						<hr>
						<p>
							You don't necessarily need business experience to purchase one of our business opportunities. We provide you with all training and marketing assistance you need to operate your business successfully.
						</p>
						<a href="/business-opportunities" class="btn btn-default">
							GET STARTED
						</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box two">
						<h2>Benefit from our experience</h2>
						<hr>
						<p>
							With over one hundered agents across our client portfolio, we have gained extensive experience within the industry. We have created workflows and operating procudures that all our agents stand to benefit from.
						</p>
						<a href="/about-us" class="btn btn-default">
							READ MORE
						</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box three">
						<h2>Depend on us for support</h2>
						<hr>
						<p>
							Our businesses are packaged as turnkey solutions with support on all levels. We provide  ongoing guidance through the constant development of custom software and use of reliable and proven digital marketing practices.
						</p>
						<a href="/our-support" class="btn btn-default">
							READ MORE
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="testimonials">
	<div class="container text-center">

		<h2 class="heading">Testimonial</h2>
		<hr class="line">
		<p class="paragraph">
			&ldquo;I have had my Skipgo agency for the last year. In this time I managed to grow my business with an additional trailer and a total of 29 skips in my fleet. Andrew and Skipgo have given me the support and advice when needed to assist me in growing my business. The trailers and skips are made with the user in mind and the ease of using the trailers and skips are evident in my daily workflow. The quality is exceptional. And in the event of something going wrong with the equiptment, there is a support team in place that understands that your business cannot stand still for any period of time and therefor sort the problem out as soon as possible. There are always new ideas and support systems being generated and all agents in the Skipgo family can enjoy this benefit. The Skipgo family has grown in such a manner that you have a lot of wisdom and ideas that is shared between agents and head office staff. I can comfortably recommend to anybody that does not mind working hard and wants to make a success of their business to invest in a Skipgo agency, it has worked for me so it can work for you too.&rdquo;
		</p>

	</div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php' ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_bot.php' ?>
