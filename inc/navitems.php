<li <?php if($pageTitle == "Home") { ?>class="active"<?php } ?>><a href="/">Home</a></li>

<li <?php if($pageTitle == "About Us") { ?>class="active"<?php } ?>><a href="/about-us">About Us</a></li>

<li <?php if($pageTitle == "Business Opportunities") { ?>class="active"<?php } ?>><a href="/business-opportunities">Business Opportunities</a></li>

<li <?php if($pageTitle == "Our Support") { ?>class="active"<?php } ?>><a href="/our-support">Our Support</a></li>

<li <?php if($pageTitle == "Contact Us") { ?>class="active"<?php } ?>><a href="/contact-us">Contact Us</a></li>
