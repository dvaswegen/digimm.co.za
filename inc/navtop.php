<div class="container visible-md visible-lg navigation-bar" data-parallax="1.2">

	<div class="pull-left logo-container">
		<a href="/">
			<img src="/img/logo-w.png" id="logo_main">
		</a>
	</div>
	<div class="pull-right nav-container">
		<div class="text-right link-container">
			<a href="tel:+27870125095" class="a">
				<span class="fa fa-phone-square"></span> 087 012 5095
			</a>
			<a href="#">
				<span class="fa fa-facebook-square"></span>
			</a>
			<a href="#">
				<span class="fa fa-google-plus-square"></span>
			</a>
		</div>
		<ul class="nav">
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navitems.php' ?>
		</ul>
	</div>

</div>

<div class="container visible-xs visible-sm navigation-bar">
	<div class="nav-container-mobile">
		<div class=" link-container">
			<a href="#">
				<span class="fa fa-google-plus-square"></span>
			</a>
			<a href="#">
				<span class="fa fa-facebook-square"></span>
			</a>
			<a href="tel:+27870125095" class="a">
				<span class="fa fa-phone-square"></span> 087 012 5095
			</a>
		</div>
	</div>
	<div class="pull-right burger">
		<a href="#"><span class="fa fa-bars"></span></a>
	</div>
	<a href="/">
		<img src="/img/logo-w.png" id="logo_mobile">
	</a>
</div>