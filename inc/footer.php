
<section id="footer" class="noverflow" data-stellar-background-ratio="0.8" data-stellar-vertical-offset="0">
	<div id="footercontent">
		<div class="container">
			<div class="row">

				<div class="col-md-4">
					<div class="footer-links">
						<ul class="unstyled">
							<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navitems.php' ?>
						</ul>
					</div>
				</div>

				<div class="col-md-4">
					<div class="footer-links">
						<div>087 012 5095</div>
						<div>
							<a href="mailto:info@digimm.co.za">info@digimm.co.za</a>
						</div>
						<div>
							<a href="#">
								<span class="fa fa-facebook-square"></span>
							</a>
							&nbsp;&nbsp;
							<a href="#">
								<span class="fa fa-google-plus-square"></span>
							</a>
						</div>
						<div>
							<a href="#">Terms and conditions</a>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<img src="/img/logo-w.png" class="logo img-responsive">
				</div>


				<div class="footer-links visible-xs">
				<br>
					<a href="#">Back to top <span class="fa fa-angle-up"></span></a>
				</div>

			</div>
		</div>
	</div>
	<div id="bottombar">
		<div class="container text-center">
			&copy; Copyright 2017 - Digimm. All rights reserved.
		</div>
	</div>
</section>
