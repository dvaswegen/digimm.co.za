
<?php 
$pageTitle = 'Contact Us';
?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_top.php' ?>


<div id="header" class="header-contact noverflow">
	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navtop.php' ?>
</div>

<section id="bo_intro">
	<div class="container text-center">
		<h2 class="heading">Thanks for getting in&nbsp;touch</h2>
		<hr class="line">
		<p>Your message was sent successfully, we will get back to you as soon as we&nbsp;can.</p>
		<a href="/" class="btn btn-primary">BACK HOME</a>
	</div>
</section>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php' ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_bot.php' ?>