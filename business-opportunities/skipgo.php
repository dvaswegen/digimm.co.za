
<?php 
$pageTitle = 'Business Opportunities';
?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_top.php' ?>


<div id="header" class="header-bo noverflow">
	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navtop.php' ?>
</div>

<section id="bo_intro">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img src="/img/bo-sg.png" class="img-responsive img-center">
				<p>
					<b>Stable income generating business opportunity</b>
				</p>
				<p>
					Skipgo is the largest mini skip drop-and-collect service in South Africa with more than 80 agencies throughout. Operators currently dispose of 20 000m³+ rubble per month, making it one of the most successful business to start.
				</p>
				<br>
				<a href="#" data-scrollto="#bo_why" class="btn btn-primary">Read More</a>
				<br>
				<br>
			</div>
			<div class="col-md-8">
				<h1>Skipgo</h1>
				<hr>
				<p>
					Our support team are all pro’s in their respective fields, passionate about the brand and will assist you to make a success, potentially making this one of the most successful businesses to start.
				</p>
				<p>
					Skipgo has become THE most recognized brand in the industry and our team of marketing and management experts have done a sterling job presenting the brand and managing its image and reputation.
				</p>
				<br>
				<style>
					.tbl {
						padding-bottom: 15px;
						margin-top: 15px;
						border-bottom: 1px solid #f0f0f0;
					}
					.tbl:last-of-type {
						border-bottom: 0;
					}
					.tbl-r {
						float: right;
					}
					@media screen and (max-width: 768px) {
						.tbl-r	{
							display: block;
							float: none;
						}
					}
				</style>
				<div class="tbl">
					<span class="tbl-l">
						<b>Purchase Price</b>
					</span>
					<span class="tbl-r">
						R 239 000,00
					</span>
				</div>
				<div class="tbl">
					<span class="tbl-l">
						<b>Asset Value (Equity on your balance sheet)</b>
					</span>
					<span class="tbl-r">
						R 199 000,00
					</span>
				</div>
				<div class="tbl">
					<span class="tbl-l">
						<b>Projected ROI (Period required to recover the capital layout)</b>
					</span>
					<span class="tbl-r">
						12 months
					</span>
				</div>
				<div class="tbl">
					<span class="tbl-l">
						<b>Projected nett monthly Income before tax</b>
					</span>
					<span class="tbl-r">
						R 39 500,00 p/m
					</span>
				</div>
				<br>

			</div>
		</div>
	</div>
</section>
<section id="bo_why" >
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Why would this business opportunity work for you?</h2>
				<hr>
				<ul class="bo-list">
					<li>
						<b>Proven to be highly profitable</b><br>
						Earn R40 000,00 + per month! (Our top performing agents are earning R 80 000,00 + pm).
					</li>
					<li>
						<b>Low Capital outlay</b><br>
						For a business with a high-income potential compared to other business opportunities, we offer you the lowest risk for the highest return on investment!
					</li>
					<li>
						<b>Low Overheads</b><br>
						Only one operator required. Fuel, vehicle & trailer maintenance are the only other main expenses. (Our robust units require very little maintenance)
					</li>
					<li>
						<b>Strongest Brand</b><br>
						Your once-off agency fee provides you with brand exclusivity to your area. Joining our brand will enable you to generate business from the word go.
					</li>
					<li>
						<b>Best of Both Worlds</b><br>
						This is not a franchise and therefore there are no royalties to be paid! You do however have all the benefits of being part of the Skipgo brand.
					</li>
					<li>
						<b>Essential Service</b><br>
						Waste management is one of the fastest growing business sectors.
					</li>
					<li>
						<b>Part Time or Full Time</b><br>
						Substitute your income by employing a driver managing the business on the side, or “owner–operate” the business and maximize the profit margins.
					</li>
					<li>
						<b>Straight Forward</b><br>
						We have packaged it as a "turnkey" business opportunity with support on all levels - you need limited business skills to make a success of it.
					</li>
					<li>
						<b>Equipment</b><br>
						The system consists of a 3 000kg GVM double axel trailer and 10 x 2m³ skips.  The system was designed for ease of use and safety in mind. It is operated by a Honda petrol motor powering a closed circuit hydraulic mechanism, including the lifting boom and stabilizing legs.  No extra labor is needed and it is a one-man operational unit.
					</li>
					<li>
						<b>Business Starter Pack</b><br>
						We provide you with the necessary printed & electronic marketing material &the start-up pack also includes onsite training on all aspects of the business.
					</li>
					<li>
						<b>Area Exclusivity</b><br>
						When you buy a Skipgo agency, you acquire the exclusive rights to operate under the registered Skipgo trademark within the boundaries of a pre-determined geographical area.
						Our marketing support services to you, is focused exclusively on your area, and no other Skipgo agent may advertise within the boundaries of your area.
						If you are satisfied with what you have read, and would like to join the Skipgo family and become part of the biggest brand in the mini-skip waste transport industry, please complete the on-page business enquire form and we will get back to you with a prospectus shortly.
					</li>
				</ul>

			</div>
		</div>
	</div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php' ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_bot.php' ?>