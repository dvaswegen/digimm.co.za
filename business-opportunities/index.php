
<?php 
$pageTitle = 'Business Opportunities';
?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_top.php' ?>


<div id="header" class="header-bo noverflow">
	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navtop.php' ?>
</div>

<section id="bo_intro">
	<div class="container text-center">
		<h2 class="heading">Business Opportunities</h2>
		<hr class="line">
		<p class="paragraph">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt placeat nemo sequi nulla voluptates, impedit quaerat nobis soluta, hic tempora, nam. Adipisci rem explicabo, tenetur autem reiciendis vero quasi, beatae.
		</p>
		<a href="#" class="btn btn-primary" data-scrollto="#bo_opportunities">
			VIEW OPPORTUNITIES
		</a>
	</div>
</section>
<section id="bo_opportunities">
	<div class="container"   data-parallax="0.8" data-parallax-offset="100">
		<div class="row">

			<div class="col-md-4">
				<div class="bo-item">
					<div class="text-center">

						<img src="/img/bo-ntt-rgb.png" class="img-responsive img-center">

						<p>
							Stable income generating business opportunity! Minimal risk, high return with superior business support structure. Reasonable overheads and no royalties with exclusivity in your area makes this an ideal business to start.
						</p>
						<a href="/business-opportunities/new-trend-trailers" class="btn btn-primary">
							VIEW
						</a>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="bo-item">
					<div class="text-center">

						<img src="/img/bo-sg.png" class="img-responsive img-center">

						<p>
							Skipgo is the largest mini skip drop-and-collect service in South Africa with more than 80 agencies throughout. Operators currently dispose of 20 000m³+ rubble per month, making it one of the most successful business to start.

						</p>
						<a href="/business-opportunities/skipgo" class="btn btn-primary">
							VIEW
						</a>

					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="bo-item">
					<div class="text-center">

						<img src="/img/bo-hidro.png" class="img-responsive img-center">

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates minus suscipit fugiat exercitationem temporibus rerum, debitis molestias adipisci aliquam maiores dolor nisi at? Fugiat beatae expedita harum in.
						</p>
						<a href="/business-opportunities/hidro" class="btn btn-primary">
							VIEW
						</a>

					</div>
				</div>
			</div>


		</div>
	</section>

	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php' ?>

	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_bot.php' ?>