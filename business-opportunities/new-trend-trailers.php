
<?php 
$pageTitle = 'Business Opportunities';
?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_top.php' ?>


<div id="header" class="header-bo noverflow">
	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navtop.php' ?>
</div>

<section id="bo_intro">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<img src="/img/bo-ntt-rgb.png" class="img-responsive img-center">
				<p>
					<b>Stable income generating business opportunity</b>
				</p>
				<p>
					Minimal risk, high return with superior business support structure. Reasonable overheads and no royalties with exclusivity in your area makes this an ideal business to start.
				</p>
				<a href="#" data-scrollto="#bo_why" class="btn btn-primary">Read More</a>
				<br>
				<br>
			</div>
			<div class="col-md-8">
				<h1>New Trend Trailers</h1>
				<hr>
				<p>
					New Trend Trailers has become THE most recognized trailer dealership and business for sale within its respective areas.Our team of marketing and management experts have done a sterling job presenting the brand and managing its image and reputation.
				</p>
				<br>
				<style>
					.tbl {
						padding-bottom: 15px;
						margin-top: 15px;
						border-bottom: 1px solid #f0f0f0;
					}
					.tbl:last-of-type {
						border-bottom: 0;
					}
					.tbl-r {
						float: right;
					}
					@media screen and (max-width: 768px) {
						.tbl-r	{
							display: block;
							float: none;
						}
					}
				</style>
				<div class="tbl">
					<span class="tbl-l">
						<b>Purchase Price</b>
					</span>
					<span class="tbl-r">
						R 550 000,00
					</span>
				</div>
				<div class="tbl">
					<span class="tbl-l">
						<b>Asset Value (Equity on your balance sheet)</b>
					</span>
					<span class="tbl-r">
						R 481 000,00
					</span>
				</div>
				<div class="tbl">
					<span class="tbl-l">
						<b>Projected ROI (Period required to recover the capital layout)</b>
					</span>
					<span class="tbl-r">
						12 to 18 months
					</span>
				</div>
				<div class="tbl">
					<span class="tbl-l">
						<b>Projected nett monthly Income before tax</b>
					</span>
					<span class="tbl-r">
						R 59 578,49 p/m
					</span>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="bo_why">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>Why would this business opportunity work for you?</h2>
				<hr>
			
				<ul class="bo-list">
					<li>
						<b>Low risk / High Return:</b><br>
						Just about every investment opportunity seems to claim this nowadays. We can back the statement!
					</li>
					<li>
						<b>Superior quality</b><br>
						Our trailer manufacturing standards are of the highest and our trailers feature unique & superior components clearly distinguishing itself from the rest of the market. 
					</li>
					<li>
						<b>Proven business support structure</b><br>
						Our unique marketing and management support structure already enjoys enormous success.
					</li>
					<li>
						<b>Best of both Worlds</b><br>
						This is not a franchise and therefore there are no royalties to be paid! You do however have all the benefits of being part of the New Trend Trailers brand.
					</li>
					<li>
						<b>Area exclusivity is our guarantee to you:</b><br>
						Your once-off agency fee provides you with brand exclusivity in your area. Our brand should enable you to generate business opportunities from the word go.
					</li>
					<li>
						<b>Reasonable overheads and no royalties:</b><br>
						Monthly expenses are relatively low. No royalties applicable and only a fixed monthly fee to provide you with our marketing & support services.
					</li>
					<li>
						<b>Collective Experience:</b><br>
						Our collective experience in the trailer hire and sales industry has taught us a few tricks. This intellectual property will be shared with you.
					</li>
					<li>
						<b>Mechanical Support:</b><br>
						We have a team of mechanical engineers who designs and develops our product range and are on call ready to deal with any mechanical queries you might have.
					</li>
					<li>
						<b>Straight Forward:</b><br>
						We have packaged it as a "turnkey" business with support on all levels - you need limited business skills to make a success of it.
					</li>
					<li>
						<b>Business Starter Pack:</b><br>
						We provide you with the necessary printed & electronic marketing material &the start-up pack also includes onsite training on all aspects of the business.
						Our marketing support services to you are focused exclusively on your area, and no other Skipgo agent may advertise within the boundaries of your area.
						If you are satisfied with what you have read, and would like to join the New Trend Trailers family, please complete the on-page business enquire form and we will get back to you with a prospectus shortly.
					</li>
				</ul>

			</div>
		</div>
	</div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php' ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_bot.php' ?>