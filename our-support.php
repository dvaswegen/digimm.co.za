
<?php 
$pageTitle = 'Our Support';
?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_top.php' ?>


<div id="header" class="header-support noverflow">
	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navtop.php' ?>
</div>

<section id="support_intro">
	<div class="container text-center">
		<h2 class="heading">Our support</h2>
		<hr class="line">
		<p class="paragraph">
			Our ongoing support structure enables small business owners to compete with the marketing budgets of bigger corporations in all of our business opportunties.
		</p>
		<a href="#" class="btn btn-primary" data-scrollto="#support_items">
			READ MORE
		</a>
	</div>
</section>
<section id="support_items">
	<div class="container text-center" data-parallax="0.9" data-parallax-offset="75">
		
		<div class="row">
			
			<div class="col-md-6">
				<div class="support-item">
					<h1 class="circle-icon">
						<span class="fa fa-code fa-2x"></span>
					</h1>
					<h3>Website</h3>
					<p class="paragraph">
						We offer ongoing site maintenance to ensure that content stays fresh and relevant, making use of best practises for search engine optimization (SEO: titles, tags, content and overall structure). We also make sure that you have significant in-bound links from highly respected external websites such as classified websites (Gumtree, Junkmail, Hotfrog, OLX, etc.)
					</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="support-item">
					<h1 class="circle-icon">
						<span class="fa fa-paint-brush fa-2x"></span>
					</h1>
					<h3>Design</h3>
					<p class="paragraph">
						We live in a culture that is rich in knowledge yet deprived of time.
						Therefore, it is often the visual identity of a brand that first catches the eyes of consumers.
						Our in-house team of designers can meticulously create any marketing collateral (social media banners, flyers, email newsletters) and corporate identity collateral (business cards, email signatures) requried.
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="container text-center">
		<div class="row">
			
			<div class="col-md-6">
				<div class="support-item">
					<h1 class="circle-icon">
						<span class="fa fa-share-alt fa-2x"></span>
					</h1>
					<h3>Social media</h3>
					<p class="paragraph">
						Included in all our package fees, we set aside paid advertising budgets to target specific audiences on various social media platforms. This increases your reach in different circles and sends new business your way.
					</p>
				</div>
			</div>

			<div class="col-md-6">
				<div class="support-item">
					<h1 class="circle-icon">
						<span class="fa fa-bar-chart fa-2x"></span>
					</h1>
					<h3>Reporting</h3>
					<p class="paragraph">
						We send out a monthly reports to each of our clients that includes statistics on all campaigns. This provides them with in-depth views and analytics of how each of the specific campaigns performed.
					</p>
				</div>
			</div>

		</div>
	</div>
</div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php' ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_bot.php' ?>