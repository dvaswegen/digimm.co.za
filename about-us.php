
<?php 
$pageTitle = 'About Us';
?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_top.php' ?>


<div id="header" class="header-about noverflow">
	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navtop.php' ?>
</div>

<section id="about_intro">
	<div class="container text-center">
		<h2 class="heading">Who we are</h2>
		<hr class="line">
		<p class="paragraph">
			Digimm is a business growth laboratory. We regularly launch new and interesting ideas using our exceptional marketing and management support blueprint. Should you have an innovative idea that could potentially grow into a successful multi-outlet business, please don’t hesitate to <a href="/contact-us">contact&nbsp;us</a>.
		</p>
		<p class="paragraph">
			Our team includes specialists in their respective fields within the digital marketing and business management industry with more than a hundred years collective experience. We are creative, energetic, and our mission is to constantly develop innovative ideas that make sense to ensure better results for our&nbsp;clients.
		</p>
	</div>
</section>
<section id="about_story" data-parallax="0.85" data-parallax-offset="20">
	<div class="container text-center">
		<h2 class="heading">Our story</h2>
		<hr class="line">
		<p class="paragraph">
			Our founder, Andrew van Zyl, has over twenty five years of experience in the small business sector. As with most successful businnesmen, Andrew has learned a lot of things the hard way. Through his experiences he however identified the opportunity for a business model that relies on ongoing support in marketing and management, combined with a high equity proposition.
		</p>
		<p class="paragraph">
			Now, decades later, this business model has been applied to multiple successful brands such as Skipgo, New Trend Trailers and Hidro, all managed and marketed by Digimm.
		</p>
	</div>
</section>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php' ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_bot.php' ?>