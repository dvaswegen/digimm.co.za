
<?php 
$pageTitle = 'Contact Us';
?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_top.php' ?>


<div id="header" class="header-contact noverflow">
	<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/navtop.php' ?>
</div>

<section id="bo_intro">
	<div class="container text-center">
		<h2 class="heading">Contact Us</h2>
		<hr class="line">
		<p class="paragraph">We would love to hear from you, please fill in the form below and we will get back to you as soon as possible</p>
		<p><br><br></p>
	</div>
	<form class="contact-form validate-me" action="/contact-us-sent" method="get">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="Full Name">Full Name</label>
						<input id="Full Name" name="Full Name" type="text" class="form-control" required>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="Email Address">Email Address</label>
						<input id="Email Address" name="Email Address" type="email" class="form-control" required>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="Phone Number">Phone Number</label>
						<input id="Phone Number" name="Phone Number" type="text" class="form-control">
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label for="Message">Message</label>
						<textarea name="Message" id="Message" style="resize: vertical;" rows="5" class="form-control" required></textarea>
					</div>
				</div>
				<div class="col-md-12 text-center">
					<div class="form-group">
						<button class="btn btn-primary">SEND</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>


<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php' ?>

<?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/_bot.php' ?>